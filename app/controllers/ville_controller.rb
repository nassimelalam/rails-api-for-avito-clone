class VilleController < ApplicationController
    def index
        @city=Ville.order(nom: :asc).all()
        render :json => @city
    end

    def create
        @city = Ville.create!(ville_params)
    end

    def ville_params
        params.require(:ville).permit(:nom)
    end
end
