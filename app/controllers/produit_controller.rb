class ProduitController < ApplicationController
    def index 
        
        @product = Produit.all

        render :json => @product ,:include=> [:ville ,:category]
    end
    
    def show 
        id_prod=request.params['id']
        @product=Produit.find(id_prod)
        render :json => @product ,:include=> [:ville ,:category]
    end

    def create 
        @product = Produit.create!(produit_params)
    end

    

    def destroy
        @product = Produit.find(params[:id])
        @product.destroy()
    end
    
    def update
        @product = Produit.find(params[:id])
        @product.update_attributes(produit_params)
    end
    

    private

    def produit_params
        params.require(:produit).permit(:title, :description,:prix, :user_id, :ville_id,:category_id)
    end


end
