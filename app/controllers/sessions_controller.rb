class SessionsController < Devise::SessionsController

private
  def respond_with(resource, _opts = {})
      render json: {:token => current_token,:resource => resource}
    end

    def respond_to_on_destroy
      head :ok
    end

    def current_token
      request.env['warden-jwt_auth.token']
    end
end
