class CategoryController < ApplicationController
    def index
        @category=Category.order(nom: :asc).all()
        render :json => @category
    end
    
    def create
        @category = Category.create!(category_params)
    end

    def category_params
        params.require(:category).permit(:nom)
    end
end
