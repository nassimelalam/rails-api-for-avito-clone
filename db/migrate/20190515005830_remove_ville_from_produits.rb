class RemoveVilleFromProduits < ActiveRecord::Migration[5.2]
  def change
    remove_column :produits, :ville, :string
  end
end
