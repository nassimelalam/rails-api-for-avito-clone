class AddUVilleToProduits < ActiveRecord::Migration[5.2]
  def change
    add_reference :produits, :ville, foreign_key: true
  end
end
