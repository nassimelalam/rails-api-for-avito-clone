class RemoveUserFromProduits < ActiveRecord::Migration[5.2]
  def change
    remove_column :produits, :user, :integer
  end
end
