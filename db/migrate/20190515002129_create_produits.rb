class CreateProduits < ActiveRecord::Migration[5.2]
  def change
    create_table :produits do |t|
      t.string :title
      t.text :description
      t.integer :categorie
      t.float :prix
      t.integer :ville
      t.integer :user

      t.timestamps
    end
  end
end
