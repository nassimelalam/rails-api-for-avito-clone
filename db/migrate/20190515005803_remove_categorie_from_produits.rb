class RemoveCategorieFromProduits < ActiveRecord::Migration[5.2]
  def change
    remove_column :produits, :categorie, :string
  end
end
