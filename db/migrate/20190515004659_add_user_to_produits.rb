class AddUserToProduits < ActiveRecord::Migration[5.2]
  def change
    add_reference :produits, :user, foreign_key: true
  end
end
