class AddUCategoriesToProduits < ActiveRecord::Migration[5.2]
  def change
    add_reference :produits, :category, foreign_key: true
  end
end
