Rails.application.routes.draw do
  get 'search_categ/:categ/:sort', to: 'produit#search_categ'
  get 'search_city/:city/:sort', to: 'produit#search_city'
  get 'search/:categ/:city/:sort', to: 'produit#search_categ_city'
  get 'produit/all/:sort/:page', to: 'produit#index', defaults: { sort: 0 }
  get 'fav',to: 'fav#myfavs'


  resources :user  , :ville , :category
  resources :produit

    devise_for :users,
               path: '',
               path_names: {
                 sign_in: 'login',
                 sign_out: 'logout',
                 registration: 'signup'
               },
               controllers: {
                 sessions: 'sessions',
                 registrations: 'registrations'
               }
    
end

